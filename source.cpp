#include <iostream>
using namespace std;

class Kit {
protected:
	int data;
	Kit *next;
public:
	virtual void print() = 0;
};

class Stack : public Kit {
private:
	Stack *come;
	Stack *next;
public:
	Stack() {
		come = this;
		next = NULL;
	}

	Stack *push(int a) {
		Stack *node;
		node = this;
		while (node->next != NULL)
			node = node->next;
		
		Stack *nodenew = new Stack;
		nodenew->come = node->come;
		node->data = a;
		node->next = nodenew;
		return node;
	}

	void print() {
		cout << "����: ";
		Stack *node = this->come;
		while (node->next != NULL) {
			cout << node->data << " ";
			node = node->next;
		}
		cout << endl << endl;
		return;
	}

	Stack *pop() {
		Stack *node;
		node = come;
		while (node->next->next != NULL)
			node = node->next;
		node->next = NULL;
		if (node == come)
			return NULL;
		return node;
	}
};

class Queue : public Kit {
private:
	Queue *come;
	Queue *next;
public:
	Queue() {
		come = this;
		next = NULL;
	}

	Queue *push(int a) {
		Queue *node;
		node = this;

		while (node->next != NULL)
			node = node->next;
		
		Queue *nodenew = new Queue;
		nodenew->come = node->come;
		node->data = a;

		node->next = nodenew;
		return node;
	}

	void print() {
		cout << "�������: ";
		Queue *node = this->come;
		while (node->next != NULL) {
			cout << node->data << " ";
			node = node->next;
		}
		cout << endl << endl;
		return;
	}

	Queue *pop() {
		if (come->next->next == NULL)
			return NULL;
		Queue *node;
		node = come;
		node->come = come->next;
		return node;
	}
};

int main() {
	setlocale(0, "Russian");
	//int mass[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
	cout << "������� ����� �������: ";
	int n;
	cin >> n;
	int *mass = new int[n];

	for (int i = 0; i < n; i++) {
		cout << "������� �������� " << i + 1 << "-�� ��������: ";
		cin >> mass[i];
	}

	Stack *st = new Stack;
	Queue *q = new Queue;
	for (int i = 0; i < n; i++) {
		st = st->push(mass[i]);
		q = q->push(mass[i]);
		st->print();
		q->print();
	}
	while (st != NULL && q != NULL ) {
		st = st->pop();
		q = q->pop();
		if (st != NULL && q != NULL)
			st->print();
			q->print();
	}
	delete mass;
	system("pause");
}